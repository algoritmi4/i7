#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <iomanip>

class Mjeranje{

public:
		//inicializacija 
		std::vector<double> VEKTOR;
		std::string line;
        std::string u_q;
        std::string coolant;
        std::string stator_winding;
        std::string u_d;
        std::string stator_tooth;
        std::string motor_speed;
        std::string i_d;
        std::string i_q;
        std::string pm,stator_yoke;
        std::string ambient;
        std::string torque;
        std::string profile_id;
	
		std::vector<std::string> stupac1;
        std::vector<std::string> stupac6;
        std::vector<std::string> stupac12;
		
        void citanje(){
			//data type s kojim se dohvaca, citaju i upisuju inf u fileove
			std::ifstream file ("measures_v2.csv");
			
			//while petlja koja dohvaca podatke pomocu getline
			while(std::getline(file, line)){
				std::stringstream linestream(line);
				std::getline(linestream, u_q, ',');
				std::getline(linestream, coolant, ',');
				std::getline(linestream, stator_winding, ',');
				std::getline(linestream, u_d, ',');
				std::getline(linestream, stator_tooth, ',');
				std::getline(linestream,motor_speed, ',');
				std::getline(linestream,i_d, ',');
				std::getline(linestream,i_q, ',');
				std::getline(linestream,pm, ',');
				std::getline(linestream,stator_yoke, ',');
				std::getline(linestream,ambient, ',');
				std::getline(linestream,torque, ',');
				std::getline(linestream,profile_id, ',');
				
				//pushanje podataka u stupce
				stupac1.push_back(u_q);
				stupac6.push_back(motor_speed);
				stupac12.push_back(profile_id);
			}
		}
			
		//funkcija za dohvacanje najvece vrijednosti u stupcu ( 1, 6, 12)
        void dohvacanjeNajvecihVrijednosti(std::vector<std::string>& vektor, int n=0){
			//pokretanje brojaca vremena
			auto start = std::chrono::high_resolution_clock::now();
            //pozivanje clera zbog dumpanje conteinera i ponovno koristenje vektora
			VEKTOR.clear();
			//dohvacanje vrijednosti i kod returna pretvaranje stringa u double
			std::transform(vektor.begin()+1, vektor.end(), std::back_inserter(VEKTOR),[](const std::string& str) { return std::stod(str); });
			//sortiranje vrijednosti (provjera da li je x > y)
			std::sort(VEKTOR.begin(),VEKTOR.end(),std::greater<>());
			//ispis fixirane vrijednosti koja ce imati 12 decimala
			if(n==0){
				std::cout << std::fixed << std::setprecision(12) <<VEKTOR[0]<<std::endl;
			}
			else if (n>0){
                for(int i=0; i<n;i++){
					std::cout << std::fixed << std::setprecision(12) <<VEKTOR[i]<<std::endl;
				}
			}
            else{
					std::cout<<"Neispravan broj!";
				}
			//zaustavljanje vremena
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            //ispis vremena potrebno za potragu MIN vrijednosti
			std::cout << "Vrijeme dohvacanja MAX vrijednosti: "<< duration.count() << " us" << std::endl;			
		}

		//funkcija za dohvacanje najmanje vrijednosti u stupcu ( 1, 6, 12)
		void dohvacanjeNajmanjihVrijednosti(std::vector<std::string>& vektor, int n=0){
			//pokretanje brojaca vremena
			auto start = std::chrono::high_resolution_clock::now();
            //pozivanje clera zbog dumpanje conteinera i ponovno koristenje vektora
			VEKTOR.clear();
			//dohvacanje vrijednosti i kod returna pretvaranje stringa u double
			std::transform(vektor.begin()+1, vektor.end(), std::back_inserter(VEKTOR),[](const std::string& str) { return std::stod(str); });
			//sortiranje vrijednosti (provjera da li je x < y)
			std::sort(VEKTOR.begin(),VEKTOR.end(),std::less<>());
			//ispis fixirane vrijednosti koja ce imati 12 decimala
			if(n==0){
				std::cout << std::fixed << std::setprecision(12) <<VEKTOR[0]<<std::endl;
			}
			else if(n>0){
				for(int i=0; i<n;i++){
					std::cout << std::fixed << std::setprecision(12) <<VEKTOR[i]<<std::endl;
				}
			}
			else{
				std::cout<<"Neispravan broj!";
			}
			//zaustavljanje vremena
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
            //ispis vremena potrebno za potragu MIN vrijednosti
			std::cout << "Vrijeme dohvacanja MIN vrijednosti: "<< duration.count() << " us" << std::endl;	
            			
		}
			
		//funkcija za pretrazivanje vrijednosti u stupcu ( 1, 6, 12)
		void pretrazivanje(std::vector<std::string>& vektor,std::vector<double>& vrijednosti){
			//pokretanje brojaca vremena
			auto start = std::chrono::high_resolution_clock::now();
            //pozivanje clera zbog dumpanje conteinera i ponovno koristenje vektora
			VEKTOR.clear();
			//dohvacanje vrijednosti i kod returna pretvaranje stringa u double
			std::transform(vektor.begin()+1, vektor.end(), std::back_inserter(VEKTOR),[](const std::string& str) { return std::stod(str); });
			//sortiranje vrijednosti (provjera da li je x < y)
			std::sort(VEKTOR.begin(),VEKTOR.end(), std::less<>());
			//sortiranje vrijednosti (provjera da li je vrijednost < y)
			std::sort(vrijednosti.begin(), vrijednosti.end(),std::less<>());

			for(auto i : vrijednosti){
				//binarna pretraga elemenata (vektor se djeli na pola, onda opet na pola i tako sve dok ne nade vrijednost i)
				if(std::binary_search(VEKTOR.begin(), VEKTOR.end(), i)){
					//kad je vrijednost pronadena fixira se 12 decimala 
					std::cout  << std::fixed << std::setprecision(12) <<"Pronadena je vrijednost: "<<i<<std::endl;
				}
				else{
					//vrijednosti nije pronadena
					std::cout << std::fixed << std::setprecision(12) << "Vrijednost " << i << " nije pronadena"<<std::endl;	
				}
		    }
			//zaustavljanje vremena
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
			//ispis vremena potrebno za potragu i vrijednosti
            std::cout << "Vrijeme pretrazivanja: "<< duration.count() << " us" << std::endl;	
            std::cout<<std::endl;
        }
			
		//funkcija za brisanje vrijednosti u stupcu ( 1, 6, 12)
		void brisanje(std::vector<std::string>& vektor,std::vector<double>& vrijednosti){
			//pokretanje brojaca vremena
			auto start = std::chrono::high_resolution_clock::now();
            //pozivanje clera zbog dumpanje conteinera i ponovno koristenje vektora
			VEKTOR.clear();
			//dohvacanje vrijednosti i kod returna pretvaranje stringa u double
			std::transform(vektor.begin()+1, vektor.end(), std::back_inserter(VEKTOR),[](const std::string& str) { return std::stod(str); });
			//for petlja koja vrti do vrijednosti koja je data i onda ju brise
			for (int i=0;i<vrijednosti.size();i++){
    			auto iterator = std::remove(VEKTOR.begin(), VEKTOR.end(), vrijednosti[i]);
    			VEKTOR.erase(iterator, VEKTOR.end());
			}
			//zaustavljanje vremena
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
			//ispis vremena potrebno za brisanje
            std::cout << "Vrijeme brisanja: "<< duration.count() << " us" << std::endl;	
		}
			
		//funkcija za dodavanje vrijednosti u stupcu ( 1, 6, 12)
        void dodavanje(std::vector<std::string>& vektor,std::vector<double>& vrijednosti){
            //pokretanje brojaca vremena
			auto start = std::chrono::high_resolution_clock::now();           
            //pozivanje clera zbog dumpanje conteinera i ponovno koristenje vektora
			VEKTOR.clear();
			//dohvacanje vrijednosti i kod returna pretvaranje stringa u double
			std::transform(vektor.begin()+1, vektor.end(), std::back_inserter(VEKTOR),[](const std::string& str) { return std::stod(str); });
			//for petlja koja pusha vrijednost na kraj vectora
			for(int i =0;i<vrijednosti.size();i++)
				VEKTOR.push_back(vrijednosti[i]);
            //zaustavljanje vremena
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
			//ispis vremena potrebno za dodavanje
            std::cout << "Vrijeme dodavanja: "<< duration.count() << " us" << std::endl;	
        }

};



int main(){
	//stvaranje objekta
 	Mjeranje mjerenja;
	
    std::vector<double> unos = {10.001023, 101.20002, -1.023};
	std::vector<double> pretraga = {24.43513272236875, 45, 24.431000809582198, 27.177768720090423, 18, 112.19752312091475, 2499.9709920281334, 2497.115077519074};
	
	//citanje datoteke
    mjerenja.citanje();
	
    auto start = std::chrono::high_resolution_clock::now();
	
    mjerenja.dohvacanjeNajvecihVrijednosti(mjerenja.stupac1);
    std::cout<<std::endl;

	mjerenja.dohvacanjeNajmanjihVrijednosti(mjerenja.stupac1);
    std::cout<<std::endl;

	mjerenja.pretrazivanje(mjerenja.stupac1, pretraga);
    mjerenja.pretrazivanje(mjerenja.stupac6, pretraga);
    mjerenja.pretrazivanje(mjerenja.stupac12, pretraga);
    std::cout<<std::endl;
	
    mjerenja.dodavanje(mjerenja.stupac1, unos);
    std::cout<<std::endl;
	
    mjerenja.brisanje(mjerenja.stupac1, unos);
    std::cout<<std::endl;
    
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	
    std::cout<<"Vrijeme: "<<duration.count()<<" us"<<std::endl;
	
	return 0;
}
